package de.mobilesol.matebet.web.exception;

public class AuthException extends MatebetException {

	private static final long serialVersionUID = 1L;

	public AuthException() {
		super();
	}

	public AuthException(int code, String message) {
		super(code, message);
	}
}
