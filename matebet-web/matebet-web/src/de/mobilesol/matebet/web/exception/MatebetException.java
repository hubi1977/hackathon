package de.mobilesol.matebet.web.exception;

import com.google.gson.Gson;

import de.mobilesol.matebet.ws.dto.ErrorDTO;

public class MatebetException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public static final int EXC_USER_NOT_FOUND = 211;

	public static final int EXC_BET_ALREADY_CLOSED = 300;
	public static final int EXC_BET_NOT_STARTED = 301;

	public static final int EXC_LOGIN_FAILED = 401;

	public static final int EXC_FORGOT_USER_NOT_FOUND = 801;
	public static final int EXC_USER_ALREADY_EXISTS = 810;
	public static final int EXC_INVALID_PASSWORD = 815;
	public static final int EXC_EMAIL_ALREADY_EXISTS = 817;
	public static final int EXC_USER_WITH_EMAIL_ALREADY_EXISTS = 818;
	public static final int EXC_FORGOT_CANNOT_RESET_EXT_USER = 820;
	public static final int EXC_USER_NAME_IS_ILLEGAL = 821;

	public static final int EXC_VERSION_UPDATE = 900;

	public int code;
	public String message;

	public MatebetException() {
	};

	public MatebetException(int code, String message) {
		super(message);
		this.code = code;
		this.message = message;
	}

	public String getJson() {
		ErrorDTO e = new ErrorDTO(code, message);
		Gson gson = new Gson();
		return gson.toJson(e);
	}

}
