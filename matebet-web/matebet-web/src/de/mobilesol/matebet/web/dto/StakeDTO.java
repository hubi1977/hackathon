package de.mobilesol.matebet.web.dto;

import java.io.Serializable;

public class StakeDTO implements Serializable {

	private static final long serialVersionUID = 355084804845249810L;

	public int stakeId;
	public String stake;

	@Override
	public String toString() {
		return "StakeDTO [stakeId=" + stakeId + ", stake=" + stake + "]";
	}
}
