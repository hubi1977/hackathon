package de.mobilesol.matebet.web.dto;

import java.io.Serializable;
import java.util.Map;

public class PushNotificationDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public Boolean ignoreIos, ignoreAndroid;
	public String title, message;
	public Page page;
	public Map<String, Object> payload;

	public PushNotificationDTO() {
	};

	public PushNotificationDTO(String title, String message, Page page,
			Map<String, Object> payload) {
		this.title = title;
		this.message = message;
		this.page = page;
		this.payload = payload;
	}

	@Override
	public String toString() {
		return "PushNotificationDTO [title=" + title + ", message=" + message
				+ ", page=" + page + ", payload=" + payload + "]";
	}

	public static enum Page {
		CLOSED_BETS("app.closedbets"), OPEN_BETS("app.openbets"), FRIENDS(
				"app.myfriends"), BET_STATUS("app.status"), MAIN("app.main");
		public String value;

		private Page(String value) {
			this.value = value;
		}
	};
}
