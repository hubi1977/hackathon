package de.mobilesol.matebet.web.service.googleplus.dto;

import java.util.List;

public class GoogleplusFriendDTO {
	public List<GpDTO> items;

	public static class GpDTO {
		public String objectType, id, displayName;
	}
}
