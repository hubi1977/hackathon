package de.mobilesol.matebet.web.service;

import java.util.List;

import org.apache.log4j.Logger;

import de.mobilesol.matebet.web.dao.ContactDAO;
import de.mobilesol.matebet.web.dto.contact.ContactDTO;

public class ContactService {
	private static final Logger log = Logger
			.getLogger(ContactService.class.getName());
	private ContactDAO dao = new ContactDAO();

	public void updateContacts(int userId, List<ContactDTO> contacts) {
		log.info("updateContacts(" + userId + ", " + contacts.size() + ")");
		dao.updateContacts(userId, contacts);
	}

	public List<ContactDTO> getContacts(int userId) {
		log.info("getContacts(" + userId + ")");
		return dao.getContacts(userId);
	}
}
