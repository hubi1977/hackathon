package de.mobilesol.matebet.web.service;

import java.lang.reflect.Type;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import de.mobilesol.matebet.web.dao.PushHistoryDAO;
import de.mobilesol.matebet.web.dto.BetDTO;
import de.mobilesol.matebet.web.dto.PushNotificationDTO;

public class PushHistoryService {

	private static final Logger log = Logger
			.getLogger(PushHistoryService.class.getName());

	private PushHistoryDAO dao = new PushHistoryDAO();

	public void addPushNotification(int userId, PushNotificationDTO push,
			String platform) {
		dao.addPushNotification(userId, push, platform);
	}

	public List<PushNotificationDTO> getPushNotifications(int userId) {
		return dao.getPushNotifications(userId);
	}

	public PushNotificationDTO getPushNotification(String key) {
		PushNotificationDTO push = dao.getPushNotification(key);

		if (push.payload != null) {
			Object o = push.payload.get("bets");
			log.info("o=" + o + ";" + o.getClass());
			if (o != null) {
				Type ab = new TypeToken<List<BetDTO>>() {
				}.getType();
				List<BetDTO> bets = new Gson().fromJson(o.toString(), ab);
				push.payload.put("bets", bets);
				log.info("found bets " + bets.size() + ";" + bets);
			}
			log.info("found ush with " + o);
		}

		return push;
	}
}
