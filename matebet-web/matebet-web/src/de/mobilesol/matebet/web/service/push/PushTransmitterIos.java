package de.mobilesol.matebet.web.service.push;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import apns.ApnsConnection;
import apns.ApnsConnectionFactory;
import apns.ApnsException;
import apns.DefaultApnsConnectionFactory;
import apns.DefaultFeedbackService;
import apns.DefaultPushNotificationService;
import apns.FailedDeviceToken;
import apns.FeedbackService;
import apns.PushNotification;
import apns.PushNotificationService;
import apns.keystore.ClassPathResourceKeyStoreProvider;
import apns.keystore.KeyStoreProvider;
import apns.keystore.KeyStoreType;
import de.mobilesol.matebet.web.dto.BetDTO;
import de.mobilesol.matebet.web.dto.PushNotificationDTO;
import de.mobilesol.matebet.web.dto.PushRecipientDTO;

public class PushTransmitterIos extends AbstractPushTransmitter {

	private static final Logger log = Logger
			.getLogger(PushTransmitterIos.class.getName());

	public PushTransmitterIos() {
		super.platform = "Ios";
	}

	@Override
	protected List<TransmitterResult> sendPushInternal(
			List<PushRecipientDTO> list, PushNotificationDTO push) {

		if (list.isEmpty()) {
			return new ArrayList<>();
		}

		log.info("sending to ios");
		List<TransmitterResult> result = new ArrayList<TransmitterResult>();

		PushNotificationService pns = new DefaultPushNotificationService();

		DefaultApnsConnectionFactory.Builder builder = DefaultApnsConnectionFactory.Builder
				.get();
		// if (usingProductionApns) {
		KeyStoreProvider ksp = new ClassPathResourceKeyStoreProvider(
				"LuiPushPNProdCertificates.p12", KeyStoreType.PKCS12,
				new char[]{});
		builder.setProductionKeyStoreProvider(ksp);

		ApnsConnection pushConnection = null;
		ApnsConnection feedbackConnection = null;
		ApnsConnectionFactory acf;
		try {
			acf = builder.build();
			log.info("openpush connection");
			pushConnection = acf.openPushConnection();
			feedbackConnection = acf.openFeedbackConnection();

		} catch (ApnsException ex) {
			log.info("failed", ex);
			TransmitterResult tr = new TransmitterResult();
			result.add(tr);
			tr.userId = list.get(0).userId;
			tr.ok = false;

			return result;
		}

		for (PushRecipientDTO p : list) {
			TransmitterResult tr = new TransmitterResult();
			result.add(tr);
			tr.userId = p.userId;
			tr.token = p.token;
			tr.ok = true;

			PushNotification pn = new PushNotification().setAlert(push.title)
					.setBadge(1).setDeviceTokens(p.token);

			Map<String, Object> payload = new HashMap<String, Object>();
			if (push.payload != null) {
				payload.putAll(push.payload);
			}
			if (push.page != null) {
				payload.put("page", push.page.value);
			}

			for (Map.Entry<String, Object> e : payload.entrySet()) {
				log.info("add " + e.getKey() + ":" + e.getValue());
				pn.setCustomPayload(e.getKey(), toJsonObject(e.getValue()));
			}
			pn.setCustomPayload("platform", "ios");
			pn.setCustomPayload("message", push.message);

			// TODO: title can be removed with v>=1.2.7
			pn.setCustomPayload("title", push.message);

			/*
			 * } else { KeyStoreProvider ksp = new
			 * ClassPathResourceKeyStoreProvider(
			 * "apns/apns_certificates_sandbox.p12", KeyStoreType.PKCS12,
			 * KEYSTORE_PASSWORD); builder.setSandboxKeyStoreProvider(ksp); }
			 */
			try {
				log.info("sending ..." + pn.getCustomPayload());
				pns.send(pn, pushConnection);

				log.info("now read the feedback");
				FeedbackService fs = new DefaultFeedbackService();
				List<FailedDeviceToken> failedTokens = fs
						.read(feedbackConnection);

				for (FailedDeviceToken failedToken : failedTokens) {
					log.info("ts=" + failedToken.getFailTimestamp());
					log.info("token=" + failedToken.getDeviceToken());
					tr.text = "failed: " + failedToken.getFailTimestamp();
					tr.ok = false;
				}

			} catch (ApnsException ex) {
				log.error("failed", ex);
				tr.text = "failed exc: " + ex.getMessage();
				tr.ok = false;
			}
		}

		return result;
	}

	private Object toJsonObject(Object obj) {
		if (obj == null) {
			return null;
		}

		if (obj instanceof List) {
			log.info("found list");
			JSONArray a = new JSONArray();
			for (Object o : (List) obj) {
				a.put(toJsonObject(o));
			}

			return a;
		} else if (obj instanceof BetDTO) {
			log.info("found betdto");
			BetDTO b = (BetDTO) obj;
			JSONObject a = new JSONObject();
			a.put("betId", b.betId);
			a.put("title", b.title);
			a.put("stake", b.stake);
			a.put("status", b.status);
			a.put("leagueId", b.leagueId);
			a.put("redeemed", b.redeemed);

			a.put("userIds", new JSONArray(b.userIds));
			a.put("userNames", new JSONObject(b.userNames));
			a.put("resultByUser", new JSONObject(b.resultByUser));

			return a;
		} else {
			log.info("type=" + obj.getClass());
		}
		return obj;
	}
}
