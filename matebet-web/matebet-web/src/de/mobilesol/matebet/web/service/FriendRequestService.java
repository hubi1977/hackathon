package de.mobilesol.matebet.web.service;

import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import de.mobilesol.matebet.web.dao.FriendRequestDAO;

public class FriendRequestService {
	private static final Logger log = Logger
			.getLogger(FriendRequestService.class.getName());

	private FriendRequestDAO dao = new FriendRequestDAO();

	public void addFriendRequest(int userId, int friendId) {
		log.info("addFriendRequest(" + userId + ", " + friendId + ")");
		dao.addFriendRequest(userId, friendId);
	}

	public void removeFriendRequest(int userId, int friendId) {
		log.info("removeFriendRequest(" + userId + ", " + friendId + ")");
		dao.removeFriendRequest(userId, friendId);
	}

	public List<Integer> getFriendRequests(int userId) {
		log.info("getFriendRequests(" + userId + ")");
		return dao.getFriendRequests(userId);
	}

	public Set<Integer> getFriendRequestsByUser(int userId) {
		log.info("getFriendRequestsByUser(" + userId + ")");
		return dao.getFriendRequestsByUser(userId);
	}
}
