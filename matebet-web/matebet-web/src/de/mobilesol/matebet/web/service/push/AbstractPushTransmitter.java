package de.mobilesol.matebet.web.service.push;

import java.util.List;

import org.apache.log4j.Logger;

import de.mobilesol.matebet.web.dto.PushNotificationDTO;
import de.mobilesol.matebet.web.dto.PushRecipientDTO;
import de.mobilesol.matebet.web.service.PushHistoryService;
import de.mobilesol.matebet.ws.dto.ErrorDTO;

public abstract class AbstractPushTransmitter {

	private PushHistoryService historyService = new PushHistoryService();
	protected String platform;

	private static final Logger log = Logger
			.getLogger(AbstractPushTransmitter.class.getName());

	public final Object sendPush(List<PushRecipientDTO> list,
			PushNotificationDTO push) {

		if (!platform.equals("Ios") && !platform.equals("Android")) {
			throw new IllegalStateException("illlegal platform " + platform);
		}

		if (push.ignoreIos != null && push.ignoreIos
				&& platform.equals("Ios")) {
			log.info("ignore ios pushes");
			return "ignored";
		}

		if (push.ignoreAndroid != null && push.ignoreAndroid
				&& platform.equals("Android")) {
			log.info("ignore android pushes");
			return "ignored";
		}

		if (list.isEmpty()) {
			return null;
		}

		try {
			List<TransmitterResult> o = sendPushInternal(list, push);
			historyService.addPushNotification(list.get(0).userId, push,
					platform);
			return o;
		} catch (Exception e) {
			log.warn("push exception: ", e);
			ErrorDTO error = new ErrorDTO(500, e.getMessage());
			return error;
		}
	}

	abstract protected List<TransmitterResult> sendPushInternal(
			List<PushRecipientDTO> list, PushNotificationDTO push);

	public static class TransmitterResult {
		public String token;
		public int userId;
		public boolean ok;
		public Object text;
	}
}
