package de.mobilesol.matebet.web.dao;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.gson.Gson;

import de.mobilesol.matebet.web.dto.BetDTO;
import de.mobilesol.matebet.web.dto.StatisticDTO;

public class StatisticDAO {
	private static final Logger log = Logger
			.getLogger(StatisticDAO.class.getName());
	private static final String TABLE = "statistic_t";

	public StatisticDTO getStatistic(int userId) {
		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey(TABLE, userId);
		StatisticDTO stats = null;
		try {
			Entity e = datastore.get(key);
			stats = toObject(e);

		} catch (EntityNotFoundException ex) {
			log.warn("entity with key " + key + " does not exist.");
			stats = new StatisticDTO();
		}

		return stats;
	}

	public void updateStaistic(BetDTO bet) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		for (int userId : bet.userIds) {
			Integer res = bet.resultByUser.get(userId);
			if (res == null) {
				log.warn("there is no result for user " + userId + " and bet "
						+ bet);
			} else {

				Key key = KeyFactory.createKey(TABLE, userId);
				StatisticDTO stats = null;
				Entity e = null;
				try {
					e = datastore.get(key);
					stats = toObject(e);

				} catch (EntityNotFoundException ex) {
					log.warn("entity with key " + key + " does not exist.");
					e = new Entity(key);
					stats = new StatisticDTO();
				}

				if (res < 0) {
					stats.lostBetsCount++;
				} else if (res > 0) {
					stats.wonBetsCount++;
				} else {
					stats.remisBetsCount++;
				}

				stats.lostBets = null;
				stats.wonBets = null;
				stats.remisBets = null;
				stats.waitingBets = null;

				e = toEntity(e, stats);
				datastore.put(e);
			}
		}
	}

	private StatisticDTO toObject(Entity e) {
		Gson gson = new Gson();
		String stats = (String) e.getProperty("stats");
		StatisticDTO b = (stats != null
				? gson.fromJson(stats, StatisticDTO.class)
				: new StatisticDTO());

		return b;
	}

	private Entity toEntity(Entity e, StatisticDTO stats) {
		Gson gson = new Gson();
		e.setProperty("stats", gson.toJson(stats));
		return e;
	}
}
