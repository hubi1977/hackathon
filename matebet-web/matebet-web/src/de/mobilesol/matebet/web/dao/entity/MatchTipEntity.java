package de.mobilesol.matebet.web.dao.entity;

import java.util.Map;

import de.mobilesol.matebet.web.dto.MatchTipDTO.TipsByMatchMap;

public class MatchTipEntity {
	public int betId;
	public int userId;

	// tips by group
	public Map<Integer, TipsByMatchMap> groups;
}
