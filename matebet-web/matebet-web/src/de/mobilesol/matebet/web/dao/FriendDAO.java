package de.mobilesol.matebet.web.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;

public class FriendDAO {
	private static final Logger log = Logger
			.getLogger(FriendDAO.class.getName());
	private static final String TABLE = "user_x";

	public boolean addFriend(int userId, int friendId, boolean forceAdd) {
		log.info("addFriend(" + userId + ", " + friendId + ")");

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey(TABLE, userId + "," + friendId);

		if (!forceAdd) {
			try {
				datastore.get(key);
				// entity already exists, so do not overwrite anything
				return false;
			} catch (EntityNotFoundException e1) {
				// entity does not exist, so add in next step
			}
		}

		// entity does not exist. So add
		Entity e = new Entity(key);
		e.setProperty("userId", userId);
		e.setProperty("friendId", friendId);
		e.setProperty("created", new Date());
		datastore.put(e);

		return true;
	}

	public boolean isDeleted(int userId, int friendId) {
		log.info("getFriend(" + userId + ", " + friendId + ")");

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey(TABLE, userId + "," + friendId);
		try {
			Entity e = datastore.get(key);
			Boolean b = (Boolean) e.getProperty("deleted");
			if (b != null && b) {
				return true;
			}
		} catch (EntityNotFoundException e1) {
			// ignore
		}

		return false;
	}

	public void removeFriend(int userId, int friendId) {
		log.info("removeFriend(" + userId + ", " + friendId + ")");

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey(TABLE, userId + "," + friendId);
		try {
			Entity e = datastore.get(key);
			e.setProperty("deleted", true);
			datastore.put(e);
		} catch (EntityNotFoundException e) {
			log.warn("friend does not exist. So cannot be deleted");
		}
	}

	public List<Integer> getFriends(int userId) {
		log.info("getFriends(" + userId + ")");

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		q.setFilter(
				new FilterPredicate("userId", FilterOperator.EQUAL, userId));
		PreparedQuery pq = datastore.prepare(q);

		Iterator<Entity> it = pq.asIterator();
		List<Integer> friendIds = new ArrayList<Integer>();
		while (it.hasNext()) {
			Entity e = it.next();
			Number n = (Number) e.getProperty("friendId");
			Boolean b = (Boolean) e.getProperty("deleted");
			if (b == null || !b) {
				// do not add deleted friends
				friendIds.add(n.intValue());
			}
		}

		return friendIds;
	}
}
