package de.mobilesol.matebet.web.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;

public class HashUtil {

	private static final Logger log = Logger
			.getLogger(HashUtil.class.getName());

	public static Object decode(byte[] b)
			throws IOException, ClassNotFoundException {
		ByteArrayInputStream i = new ByteArrayInputStream(b);
		ObjectInputStream in = new ObjectInputStream(i);
		Object o = in.readObject();
		in.close();
		return o;
	}

	public static byte[] encode(Object object) throws IOException {
		ByteArrayOutputStream o = new ByteArrayOutputStream();
		ObjectOutputStream out = new ObjectOutputStream(o);
		out.writeObject(object);
		out.close();
		byte[] buf = o.toByteArray();
		return buf;
	}

	public static String md5(byte[] b) throws IOException {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");

			InputStream in = new ByteArrayInputStream(b);
			byte[] buffer = new byte[4096];
			int nread = 0;
			while ((nread = in.read(buffer)) >= 0) {
				md.update(buffer, 0, nread);
			}
			byte[] mdbytes = md.digest();

			// convert the byte to hex format method 1
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < mdbytes.length; i++) {
				sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16)
						.substring(1));
			}

			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			log.warn("failed ", e);
			throw new IOException(e);
		}
	}
}
