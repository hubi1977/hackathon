package de.mobilesol.matebet.ws.handler;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import de.mobilesol.matebet.web.dto.StakeDTO;
import de.mobilesol.matebet.web.service.StakeService;

public class StakeEndpoint {

	private static final long serialVersionUID = 1L;

	private static final Logger log = Logger
			.getLogger(StakeEndpoint.class.getName());

	private StakeService stakeService = new StakeService();

	public List<StakeDTO> getStakes(List<Integer> userIds,
			HttpServletResponse resp) throws IOException {
		return stakeService.getStakes(userIds);
	}
}
