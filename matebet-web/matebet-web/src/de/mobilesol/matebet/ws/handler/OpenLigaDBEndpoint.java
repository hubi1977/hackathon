package de.mobilesol.matebet.ws.handler;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import de.mobilesol.matebet.web.dto.GroupDTO;
import de.mobilesol.matebet.web.dto.LeagueDTO;
import de.mobilesol.matebet.web.dto.MatchDTO;
import de.mobilesol.matebet.web.dto.SyncOpenLigaDBResultDTO;
import de.mobilesol.matebet.web.dto.TeamDTO;
import de.mobilesol.matebet.web.service.external.OpenLigaDBService;
import de.mobilesol.matebet.web.service.external.SyncOpenLigaDBService;

public class OpenLigaDBEndpoint {

	private static final long serialVersionUID = 1L;

	private static final Logger log = Logger
			.getLogger(OpenLigaDBEndpoint.class.getName());

	private OpenLigaDBService service = new OpenLigaDBService();
	private SyncOpenLigaDBService oservice = new SyncOpenLigaDBService();

	public List<LeagueDTO> handleLeagues(HttpServletResponse resp)
			throws IOException {
		List<LeagueDTO> u = service.getLeagues();
		return u;
	}

	public List<TeamDTO> handleTeams(int leagueId, HttpServletResponse resp)
			throws IOException {
		List<TeamDTO> u = service.getTeams(leagueId);
		return u;
	}

	public List<GroupDTO> handleGetGroups(int leagueId,
			HttpServletResponse resp) throws IOException {
		List<GroupDTO> groups = service.getGroups(leagueId);
		return groups;
	}

	public List<MatchDTO> handleGetMatches(int leagueId, int groupId,
			HttpServletResponse resp) throws IOException {
		List<MatchDTO> matches = service.getMatches(leagueId, groupId);
		return matches;
	}

	public SyncOpenLigaDBResultDTO fixLeague(int leagueId, boolean change,
			HttpServletResponse resp) throws IOException {
		SyncOpenLigaDBResultDTO res = oservice.fixLeague(leagueId, change);
		return res;
	}
}
