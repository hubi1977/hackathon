package de.mobilesol.matebet.ws.dto;

public class ErrorDTO {
	public int code;
	public String description;

	public ErrorDTO() {
	}

	public ErrorDTO(int code, String description) {
		this.code = code;
		this.description = description;
	}

	@Override
	public String toString() {
		return "ErrorDTO [code=" + code + ", description=" + description + "]";
	}
}
