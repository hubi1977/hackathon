package de.mobilesol.matebet.ws;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.utils.SystemProperty;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import de.mobilesol.matebet.web.dto.BetDTO;
import de.mobilesol.matebet.web.dto.BetGroupDTO;
import de.mobilesol.matebet.web.dto.BetMatchDTO;
import de.mobilesol.matebet.web.dto.FriendDTO;
import de.mobilesol.matebet.web.dto.PushNotificationDTO;
import de.mobilesol.matebet.web.dto.PushRecipientDTO;
import de.mobilesol.matebet.web.dto.PushRecipientDTO.DeviceDTO;
import de.mobilesol.matebet.web.dto.UserDTO;
import de.mobilesol.matebet.web.exception.MatebetException;
import de.mobilesol.matebet.web.exception.MatebetValidationException;
import de.mobilesol.matebet.web.service.facebook.dto.FacebookAccessTokenDTO;
import de.mobilesol.matebet.web.service.googleplus.dto.GoogleplusAccessTokenDTO;
import de.mobilesol.matebet.web.service.twitter.dto.TwitterAccessTokenDTO;
import de.mobilesol.matebet.web.util.SendMail;
import de.mobilesol.matebet.ws.handler.ActiveBetEndpoint;
import de.mobilesol.matebet.ws.handler.BetEndpoint;
import de.mobilesol.matebet.ws.handler.ClosedBetEndpoint;
import de.mobilesol.matebet.ws.handler.FriendEndpoint;
import de.mobilesol.matebet.ws.handler.GroupEndpoint;
import de.mobilesol.matebet.ws.handler.LeagueEndpoint;
import de.mobilesol.matebet.ws.handler.MatchEndpoint;
import de.mobilesol.matebet.ws.handler.MatchTipEndpoint;
import de.mobilesol.matebet.ws.handler.OpenLigaDBEndpoint;
import de.mobilesol.matebet.ws.handler.PushEndpoint;
import de.mobilesol.matebet.ws.handler.StakeEndpoint;
import de.mobilesol.matebet.ws.handler.StatisticEndpoint;
import de.mobilesol.matebet.ws.handler.TeamEndpoint;
import de.mobilesol.matebet.ws.handler.UserEndpoint;
import de.mobilesol.matebet.ws.handler.UtilEndpoint;

public class DispatcherEndpoint extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger log = Logger
			.getLogger(DispatcherEndpoint.class.getName());

	private BetEndpoint betEndpoint = new BetEndpoint();
	private ActiveBetEndpoint activeBetEndpoint = new ActiveBetEndpoint();
	private ClosedBetEndpoint closedBetEndpoint = new ClosedBetEndpoint();
	private FriendEndpoint friendEndpoint = new FriendEndpoint();
	private GroupEndpoint groupEndpoint = new GroupEndpoint();
	private LeagueEndpoint leagueEndpoint = new LeagueEndpoint();
	private MatchEndpoint matchEndpoint = new MatchEndpoint();
	private MatchTipEndpoint matchTipEndpoint = new MatchTipEndpoint();
	private TeamEndpoint teamEndpoint = new TeamEndpoint();
	private UserEndpoint userEndpoint = new UserEndpoint();
	private StatisticEndpoint statsEndpoint = new StatisticEndpoint();
	private PushEndpoint pushEndpoint = new PushEndpoint();
	private StakeEndpoint stakeEndpoint = new StakeEndpoint();
	private OpenLigaDBEndpoint openLigaDBEndpoint = new OpenLigaDBEndpoint();
	private UtilEndpoint utilEndpoint = new UtilEndpoint();

	private Gson gson = new GsonBuilder().setPrettyPrinting()
			.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ").create();

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		String user = req.getHeader("X-User");
		String userId = req.getHeader("X-UserId");
		String version = req.getHeader("X-Version");
		String userAgent = req.getHeader("User-Agent");
		String lang = req.getHeader("Accept-Language");

		if (user != null && user.toLowerCase().contains("lui")) {
			/*
			 * try { Thread.sleep(5000); } catch (InterruptedException e) { //
			 * TODO Auto-generated catch block e.printStackTrace(); }
			 */
		}
		log.info("request by: " + user + " (" + version + "); lang=" + lang);

		// remove later
		if (version != null && (version.contains("1.1.")
				|| version.contains("1.0.") || version.contains("0.0.2"))) {
			throw new MatebetException(MatebetException.EXC_VERSION_UPDATE,
					"Illegal version. Please update from https://play.google.com/apps/testing/de.mobilesol.matebet");
		}

		if (userId != null) {
			MDC.put("user", userId);
		} else if (user != null) {
			if (user.indexOf(";") > 0) {
				MDC.put("user", user.substring(0, user.indexOf(";")));
			} else {
				MDC.put("user", user);
			}
		} else {
			log.error(
					"user is not set. This should be checked. ua=" + userAgent);
			MDC.put("user", "-");
		}

		super.service(req, res);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String uri = req.getRequestURI();
		Object json = null;
		if (uri.matches("^/rs/version$")) {
			Map<String, String> map = new HashMap<String, String>();

			String applicationVersion = SystemProperty.applicationVersion.get();
			String applicationId = SystemProperty.applicationId.get();

			Date uploadDate = new Date(Long
					.parseLong(applicationVersion
							.substring(applicationVersion.lastIndexOf(".") + 1))
					/ (2 << 27) * 1000);
			map.put("version", applicationVersion);
			map.put("appId", applicationId);
			map.put("deployed", uploadDate.toString());
			map.put("currentdate", new Date().toString());
			json = map;
		} else if (uri.matches("^/rs/util/clean$")) {
			String ox = utilEndpoint.handleClean(resp).toString();
			resp.getWriter().write(ox);
			return;
		} else if (uri.matches("^/rs/repair$")) {
			// new BetDAO().repair();

		} else if (uri.matches("^/rs/bet/get/[0-9]+$")) {
			int betId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));

			json = betEndpoint.handleGetBet(betId, resp);
		} else if (uri.matches("^/rs/activebet/open/list/[0-9]+$")) {
			int userId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));

			json = activeBetEndpoint.handleOpenBets(userId, resp);
		} else if (uri.matches("^/rs/activebet/open/get/[0-9]+/[0-9]+$")) {
			int userId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			uri = uri.substring(0, uri.lastIndexOf("/"));
			int betId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			json = activeBetEndpoint.handleOpenBet(betId, userId, resp);

		} else if (uri.matches("^/rs/closedbet/list/[0-9]+$")) {
			int userId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			json = closedBetEndpoint.handleClosedBets(userId, resp);
		} else if (uri.matches("^/rs/closedbet/get/[0-9]+/[0-9]+$")) {
			int userid = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			uri = uri.substring(0, uri.lastIndexOf("/"));
			int betId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			json = closedBetEndpoint.handleClosedBet(betId, userid, resp);
		} else if (uri.matches("^/rs/friend/list/[0-9]+$")) {
			int id = Integer.parseInt(uri.substring(uri.lastIndexOf("/") + 1));

			json = friendEndpoint.handleGetFriends(id, resp);
		} else if (uri.matches("^/rs/league/list$")) {
			boolean disabled = Boolean
					.parseBoolean(req.getParameter("disabled"));
			json = leagueEndpoint.handleGetAllLeagues(resp, disabled);
		} else if (uri.matches("^/rs/group/get/[0-9]+$")) {
			int groupId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));

			json = groupEndpoint.handleGet(groupId, resp);
		} else if (uri.matches("^/rs/group/list/[0-9]+$")) {
			int leagueId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			boolean onlyActive = ("active".equals(req.getParameter("filter")));
			json = groupEndpoint.handleGetAllByLeague(leagueId, onlyActive,
					resp);
		} else if (uri.matches("^/rs/match/get/[0-9]+$")) {
			int groupId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));

			json = matchEndpoint.handleGetMatch(groupId, resp);
		} else if (uri.matches("^/rs/match/list/[0-9]+$")) {
			int groupId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));

			json = matchEndpoint.handleGetMatchesByGroup(groupId, resp);
		} else if (uri.matches("^/rs/matchtip/get/[0-9]+/[0-9]+$")) {
			int userId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			uri = uri.substring(0, uri.lastIndexOf("/"));
			int betId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));

			json = matchTipEndpoint.handleGetMatch(betId, userId, resp);
		} else if (uri.matches("^/rs/team/get/[0-9]+$")) {
			int id = Integer.parseInt(uri.substring(uri.lastIndexOf("/") + 1));

			json = teamEndpoint.handleGet(id, resp);
		} else if (uri.matches("^/rs/team/list$")) {
			// only used by test
			json = teamEndpoint.handleGetAll(resp);
		} else if (uri.matches("^/rs/user/get/id/[0-9]+$")) {
			int id = Integer.parseInt(uri.substring(uri.lastIndexOf("/") + 1));

			json = userEndpoint.handleGetUser(id, resp);
		} else if (uri.matches("^/rs/user/list$")) {
			String query = req.getParameter("q");
			// only used by test
			json = userEndpoint.handleGetAllUsers(resp, query);
		} else if (uri.matches("^/rs/user/reset$")) {
			String sid = req.getParameter("id");
			String code = req.getParameter("code");

			if (!sid.matches("^[0-9]+$") || code == null) {
				throw new MatebetValidationException(901,
						"illegal get request " + uri);
			}
			// only used by test
			userEndpoint.handleReset(Integer.parseInt(sid), code, resp);
			resp.getWriter().write("Passwort zurückgesetzt.");
			return;
		} else if (uri.matches("^/rs/friend/query/[0-9]+$")) {
			int id = Integer.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			String query = req.getParameter("q");
			// only used by test
			json = friendEndpoint.handleGetFriends(resp, id, query);
		} else if (uri.matches("^/rs/statistic/[0-9]+$")) {
			int userId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			json = statsEndpoint.handleGetStatistic(userId, resp);
		} else if (uri.matches("^/rs/stake/list$")) {
			String userIds = req.getParameter("userIds");
			if (userIds == null || !userIds.matches("^[0-9]+(,[0-9]+)*$")) {
				throw new MatebetValidationException(901,
						"illegal parameter userIds: " + uri + "?userIds="
								+ userIds);
			}
			String[] a = userIds.split(",");
			List<Integer> l = new ArrayList<Integer>();
			for (String s : a) {
				l.add(Integer.parseInt(s));
			}
			json = stakeEndpoint.getStakes(l, resp);
		} else if (uri.matches("^/rs/openligadb/leagues/list$")) {
			json = openLigaDBEndpoint.handleLeagues(resp);
		} else if (uri.matches("^/rs/openligadb/group/list/[0-9]+$")) {
			int leagueId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			json = openLigaDBEndpoint.handleGetGroups(leagueId, resp);
		} else if (uri.matches("^/rs/openligadb/team/list/[0-9]+$")) {
			int leagueId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			json = openLigaDBEndpoint.handleTeams(leagueId, resp);
		} else if (uri.matches("^/rs/openligadb/fix/[0-9]+$")) {
			int leagueId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			boolean change = Boolean.parseBoolean(req.getParameter("change"));
			json = openLigaDBEndpoint.fixLeague(leagueId, change, resp);
		} else if (uri.matches("^/rs/openligadb/match/list/[0-9]+/[0-9]+$")) {
			int groupId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			uri = uri.substring(0, uri.lastIndexOf("/"));
			int leagueId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			json = openLigaDBEndpoint.handleGetMatches(leagueId, groupId, resp);
		} else if (uri.matches("^/rs/push/android$")) {
			String user = req.getParameter("user");
			if (user == null || user.length() < 3) {
				throw new MatebetValidationException(901,
						"user is missing " + uri);
			}
			json = pushEndpoint.sendTest(user, resp);
		} else if (uri.matches("^/rs/test/email/.+$")) {
			String email = uri.substring(uri.lastIndexOf("/") + 1);
			String rr = new SendMail().send("Das ist eine Testmail",
					"<p>Hallo " + email
							+ ",</p><p>This is a text.</p><p>Cheers, Lui</p>",
					"testuser", email, null);
			resp.getWriter().write(rr);
			return;
		} else {
			throw new MatebetValidationException(901,
					"illegal get request " + uri);
		}

		resp.setContentType("application/json");
		resp.getWriter().write(gson.toJson(json));
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String uri = req.getRequestURI();
		Object json = null;
		if (uri.matches("^/rs/friend/update/[0-9]+$")) {
			int userId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			json = friendEndpoint.handleUpdateContacts(userId, req.getReader(),
					resp);
		} else if (uri.matches("^/rs/league/delete/[0-9]+$")) {
			int leagueId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));

			json = leagueEndpoint.deleteLeague(leagueId, resp);
		} else if (uri.matches("^/rs/user/login$")) {
			Gson gson = new Gson();
			UserPW u = gson.fromJson(req.getReader(), UserPW.class);

			log.info("login with " + u);
			if (u == null || u.username == null || u.password == null) {
				throw new MatebetValidationException(400,
						"you have to provide a user and password");
			}
			json = userEndpoint.handleLogin(u.username, u.password, resp);
		} else if (uri.matches("^/rs/user/login/fb$")) {

			BufferedReader reader = new BufferedReader(req.getReader());
			String line;
			StringBuffer strb = new StringBuffer();
			while ((line = reader.readLine()) != null) {
				strb.append(line + "\n");
			}

			log.info("strb=" + strb);

			Gson gson = new Gson();
			Type ab = new TypeToken<Map<String, String>>() {
			}.getType();

			Map<String, String> map = gson.fromJson(strb.toString(), ab);
			String u = map.get("data");
			if (u == null) {
				throw new MatebetValidationException(400,
						"you have to provide a user");
			}
			json = userEndpoint.handleLoginFb(
					gson.fromJson(u, FacebookAccessTokenDTO.class), resp);
		} else if (uri.matches("^/rs/user/login/googleplus$")) {

			BufferedReader reader = new BufferedReader(req.getReader());
			String line;
			StringBuffer strb = new StringBuffer();
			while ((line = reader.readLine()) != null) {
				strb.append(line + "\n");
			}

			log.info("strb=" + strb);

			Gson gson = new Gson();
			Type ab = new TypeToken<Map<String, String>>() {
			}.getType();

			Map<String, String> map = gson.fromJson(strb.toString(), ab);
			String u = map.get("data");
			if (u == null) {
				throw new MatebetValidationException(400,
						"you have to provide a user");
			}
			json = userEndpoint.handleLoginGp(
					gson.fromJson(u, GoogleplusAccessTokenDTO.class), resp);
		} else if (uri.matches("^/rs/user/login/twitter$")) {

			BufferedReader reader = new BufferedReader(req.getReader());
			String line;
			StringBuffer strb = new StringBuffer();
			while ((line = reader.readLine()) != null) {
				strb.append(line + "\n");
			}

			log.info("strb=" + strb);

			Gson gson = new Gson();
			Type ab = new TypeToken<Map<String, String>>() {
			}.getType();

			Map<String, String> map = gson.fromJson(strb.toString(), ab);
			String u = map.get("data");
			if (u == null) {
				throw new MatebetValidationException(400,
						"you have to provide a user");
			}
			json = userEndpoint.handleLoginTwitter(
					gson.fromJson(u, TwitterAccessTokenDTO.class), resp);
		} else if (uri.matches("^/rs/user/update$")) {
			Gson gson = new Gson();
			UserDTO u = gson.fromJson(req.getReader(), UserDTO.class);

			if (u == null || u.userId == 0) {
				throw new MatebetValidationException(400,
						"you have to provide a userid, ...");
			}
			log.info("update with " + u);
			json = userEndpoint.handleUpdate(u, resp);
		} else if (uri.matches("^/rs/user/forgot$")) {
			String u = req.getReader().readLine();
			log.info("login with " + u);
			if (u == null) {
				throw new MatebetValidationException(400,
						"you have to provide an email");
			}
			json = userEndpoint.handleForgot(u, resp);
		} else if (uri.matches("^/rs/push/register$")) {

			Gson gson = new Gson();
			StringBuffer strb = new StringBuffer();
			BufferedReader reader = new BufferedReader(req.getReader());
			String line;
			while ((line = reader.readLine()) != null) {
				strb.append(line + "\n");
			}
			log.info("push=\n\n" + strb + "\n\n");
			PushRecipientDTO push = new PushRecipientDTO();

			try {
				// this call will fail for version <= 1.2.2
				Push u = gson.fromJson(strb.toString(), Push.class);
				log.info("register " + u);

				if (u == null || u.userId == 0 || u.tokenID == null) {
					throw new MatebetValidationException(400,
							"illegal registration " + u);
				}

				push.token = u.tokenID;
				push.userId = u.userId;
				push.device = u.device;// u.device;
			} catch (com.google.gson.JsonSyntaxException e) {
				log.info("failed for version <= 1.2.2", e);
				Push2 u = gson.fromJson(strb.toString(), Push2.class);
				log.info("register " + u);

				if (u == null || u.userId == 0 || u.tokenID == null) {
					throw new MatebetValidationException(400,
							"illegal registration " + u);
				}

				push.token = u.tokenID;
				push.userId = u.userId;
				push.device = null;
			}

			json = pushEndpoint.registerPush(push, resp);
		} else if (uri.matches("^/rs/push/unregister$")) {

			Gson gson = new Gson();
			Push u = gson.fromJson(req.getReader(), Push.class);
			log.info("unregister " + u);

			if (u == null || u.userId == 0 || u.uuid == null) {
				throw new MatebetValidationException(400,
						"illegal registration " + u);
			}

			json = pushEndpoint.unregisterPush(u.userId, u.uuid, resp);
		} else if (uri.matches("^/rs/push/send$")) {

			Gson gson = new Gson();
			PushNotificationDTO u = gson.fromJson(req.getReader(),
					PushNotificationDTO.class);
			log.info("send " + u);

			if (u == null || u.title == null || u.message == null) {
				throw new MatebetValidationException(400,
						"illegal push notification " + u);
			}

			json = pushEndpoint.sendPush(u, resp);
		} else if (uri.matches("^/rs/push/report$")) {

			StringBuffer strb = new StringBuffer();
			BufferedReader reader = new BufferedReader(req.getReader());
			String line;
			while ((line = reader.readLine()) != null) {
				strb.append(line + "\n");
			}
			log.info("report=\n\n" + strb + "\n\n");

			json = pushEndpoint.sendReport(strb.toString(), resp);
		} else if (uri.matches("^/rs/push/android/resend$")) {
			ResendPush p = gson.fromJson(req.getReader(), ResendPush.class);
			json = pushEndpoint.resendPushTest(p.userId, p.key, resp);
		} else if (uri.matches("^/rs/friend/invite/[0-9]+/[0-9]+$")) {
			int friendId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			uri = uri.substring(0, uri.lastIndexOf("/"));
			int userId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));

			json = friendEndpoint.sendInvitationRequest(userId, friendId, resp);
		} else if (uri.matches("^/rs/friend/accept/[0-9]+$")) {
			int userId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			Gson gson = new Gson();
			FriendDTO friend = gson.fromJson(req.getReader(), FriendDTO.class);

			json = friendEndpoint.acceptInvitationRequest(userId, friend, resp);
		} else if (uri.matches("^/rs/friend/reject/[0-9]+$")) {
			int userId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			Gson gson = new Gson();
			FriendDTO friend = gson.fromJson(req.getReader(), FriendDTO.class);

			json = friendEndpoint.rejectInvitationRequest(userId, friend, resp);
		} else if (uri.matches("^/rs/friend/delete/[0-9]+/[0-9]+$")) {
			int friendId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			uri = uri.substring(0, uri.lastIndexOf("/"));
			int userId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			json = friendEndpoint.handleRemoveFriend(userId, friendId, resp);
		} else if (uri.matches("^/rs/bet/confirm/[0-9]+/[0-9]+$")) {
			int userid = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			uri = uri.substring(0, uri.lastIndexOf("/"));
			int betId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			json = betEndpoint.handleConfirmBet(betId, userid, resp);
		} else if (uri.matches("^/rs/bet/decline/[0-9]+/[0-9]+$")) {
			int userid = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			uri = uri.substring(0, uri.lastIndexOf("/"));
			int betId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			json = betEndpoint.handleDeclineBet(betId, userid, resp);
		} else if (uri.matches("^/rs/league/disable/[0-9]+$")) {
			int leagueId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			json = leagueEndpoint.disableLeague(leagueId, true);
		} else if (uri.matches("^/rs/league/enable/[0-9]+$")) {
			int leagueId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			json = leagueEndpoint.disableLeague(leagueId, false);
		} else if (uri.matches("^/rs/openligadb/import/[0-9]+$")) {
			int leagueId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));

			Queue queue = QueueFactory.getDefaultQueue();
			queue.add(TaskOptions.Builder
					.withUrl("/_ah/queue/import/" + leagueId));
			resp.getWriter().print("kicked off import job " + leagueId);

		} else if (uri.matches("^/rs/bet/addfu$")) {
			StringBuffer strb = new StringBuffer();
			BufferedReader reader = new BufferedReader(req.getReader());
			String line;
			while ((line = reader.readLine()) != null) {
				strb.append(line + "\n");
			}
			log.info("fulogs=\n\n" + strb + "\n\n");
			Gson gson = new Gson();
			FuRequest request = gson.fromJson(strb.toString(), FuRequest.class);

			String person = request.request.intent.slots.Person.get("value");
			String rising = request.request.intent.slots.Action.get("value");
			
			BetDTO bet = getBet();
			bet.userNames = new HashMap<Integer, String>();
			bet.userNames.put(1, "Fuh");
			bet.userNames.put(2, person);
			bet.title = 
					"Fuh bets 'DAX " + rising
					+ "'";
			bet.stake = request.request.intent.slots.Amount.get("value")
					+ " " + request.request.intent.slots.Betobject.get("value");
			json = betEndpoint.handleAddFu(bet, resp);
			
			String[] res = new String[]{
					"Hi Fu, Your bet has successfully been placed",
					"Hi Fu, I'm really happy that you challenged me",
					"Hi Fu, I placed your bet. I'm in love with you.",
					
			};
			FuResponse response = 
					new FuResponse(res[new Random().nextInt(res.length)]);

			log.info("res=" + gson.toJson(response));
			json = response;
		} else if (uri.matches("^/rs/bet/addphil$")) {
			StringBuffer strb = new StringBuffer();
			BufferedReader reader = new BufferedReader(req.getReader());
			String line;
			while ((line = reader.readLine()) != null) {
				strb.append(line + "\n");
			}
			log.info("phillogs=\n\n" + strb + "\n\n");
			
			String[] array = strb.toString().split("&");
			String call = null;
			String gegner = null; 
			String stock = null;

			BetDTO bet = getBet();

			for (String prt : array) {
				if (prt.startsWith("callPut=")) {
					call = prt.substring(8);
				}
				if (prt.startsWith("gegner=")) {
					gegner = prt.substring(7);
				}
				if (prt.startsWith("einsatz=")) {
					bet.stake = prt.substring(8);
				}
				if (prt.startsWith("stock=")) {
					stock = prt.substring(6);
				}
			}

			bet.userNames = new HashMap<Integer, String>();
			bet.userNames.put(1, "Phil");
			bet.userNames.put(2, gegner);

			bet.title = "Phil"
					+ " bets '" + stock + " " + call + "'";

			// callPut=steigt&dicke=titten&einsatz=20+uhr&gegner=louis&stock=dax
			
			json = betEndpoint.handleAddPhil(bet, resp);
		} else {
			throw new MatebetValidationException(902,
					"illegal post request " + uri);
		}

		resp.setContentType("application/json");
		resp.getWriter().write(gson.toJson(json));
	}
	
	private BetDTO getBet() {
		BetDTO bet = new BetDTO();
		bet.userIds = new ArrayList<Integer>();
		bet.userIds.add(1);
		bet.userIds.add(2);
		bet.leagueId = 1;
		bet.groups = new ArrayList<BetGroupDTO>();
		BetGroupDTO b = new BetGroupDTO();
		b.groupId = 1;
		b.matches = new ArrayList<BetMatchDTO>();
		bet.groups.add(b);
		BetMatchDTO m = new BetMatchDTO();
		b.matches.add(m);
		m.matchdate = new Date();
		m.matchId = 1;

		return bet;
	}
	

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String uri = req.getRequestURI();
		Object json = null;
		if (uri.matches("^/rs/logs/send$")) {
			StringBuffer strb = new StringBuffer();
			BufferedReader reader = new BufferedReader(req.getReader());
			String line;
			while ((line = reader.readLine()) != null) {
				strb.append(line + "\n");
			}
			log.info("logs=\n\n" + strb + "\n\n");
			new SendMail().send("matebet logs", "<pre>" + strb + "</pre>",
					"lui", "lui.baeumer@gmail.com", null);
		} else if (uri.matches("^/rs/bet/add$")) {
			json = betEndpoint.handleAdd(req.getReader(), resp);


		} else if (uri.matches("^/rs/closedbet/archive/[0-9]+/[0-9]+$")) {
			int userId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			uri = uri.substring(0, uri.lastIndexOf("/"));
			int betId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			json = closedBetEndpoint.handleArchived(betId, userId, resp);
		} else if (uri.matches("^/rs/closedbet/delete/[0-9]+/[0-9]+$")) {
			int userId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			uri = uri.substring(0, uri.lastIndexOf("/"));
			int betId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			json = closedBetEndpoint.handleDelete(betId, userId, resp);
		} else if (uri.matches("^/rs/friend/add$")) {
			json = friendEndpoint.handleAddFriend(req.getReader(), resp);
		} else if (uri.matches("^/rs/matchtip/add/[0-9]+/[0-9]+/[0-9]+$")) {
			int userId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			uri = uri.substring(0, uri.lastIndexOf("/"));
			int groupId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));
			uri = uri.substring(0, uri.lastIndexOf("/"));
			int betId = Integer
					.parseInt(uri.substring(uri.lastIndexOf("/") + 1));

			json = matchTipEndpoint.handleAddMatchTip(betId, groupId, userId,
					req.getReader(), resp);
		} else if (uri.matches("^/rs/user/add$")) {
			json = userEndpoint.handleAddUser(req.getReader(), resp);
		} else {
			throw new MatebetValidationException(903,
					"illegal put request " + uri);
		}

		resp.setContentType("application/json");
		resp.getWriter().write(gson.toJson(json));
	}

	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String uri = req.getRequestURI();
		Object json = null;
		if (uri.matches("^/rs/user/delete/[0-9]+$")) {
			int id = Integer.parseInt(uri.substring(uri.lastIndexOf("/") + 1));

			json = userEndpoint.handleDeleteUser(id, resp);
		} else {
			throw new MatebetValidationException(900,
					"illegal delete request " + uri);
		}

		resp.setContentType("application/json");
		resp.getWriter().write(gson.toJson(json));
	}

	private static class UserPW implements Serializable {
		private static final long serialVersionUID = 1L;

		public String username, password;

		@Override
		public String toString() {
			return "UserPW [username=" + username + ", password="
					+ (password != null ? "-" : null) + "]";
		}
	}

	public static class Push implements Serializable {
		private static final long serialVersionUID = 1L;

		public String tokenID, uuid;
		public int userId;
		public DeviceDTO device;

		@Override
		public String toString() {
			return "Push [user=" + userId + ", tokenID=" + tokenID + ", device="
					+ device + ", uuid=" + uuid + "]";
		}
	}

	public static class Push2 implements Serializable {
		private static final long serialVersionUID = 1L;

		public String tokenID;
		public int userId;

		@Override
		public String toString() {
			return "Push [user=" + userId + ", tokenID=" + tokenID + "]";
		}
	}

	public static class ResendPush implements Serializable {
		private static final long serialVersionUID = 1L;

		public String key;
		public int userId;

		@Override
		public String toString() {
			return "ResendPush [user=" + userId + ", key=" + key + "]";
		}
	}
	
	/*
	{"version":"1.0",
	"session":{"new":true,"sessionId":"SessionId.e78ee338-585f-4076-91eb-d01183fc8b9a",
		"application":{"applicationId":"amzn1.ask.skill.2874ba60-fbe2-429d-b0b9-b89c6c737d68"},
		"attributes":{},
		"user":{"userId":"amzn1.ask.account.AGMAUFNPDZWT4B7TVP3BN6AUYQE2U5FD2MJ2JHRQLMLYZWBMOG6CKPIPKFCNJ4GKJRCWSRW46RWM2JUSE7RQG4ZFTGFU6H6FGPLDN24KGFQL3XGA453M7FC22SCYTX3F7BMG243LI3NGLERPQAO7BPSYBC3U4ZI4WSPERRGDN34OIROINZK2AWZX6QYI66FO4NQRNCPHBLRNSXA"}
	},
	"request":{"type":"IntentRequest",
		"requestId":"EdwRequestId.b6073df1-878d-4f16-987f-62f719f420dd",
		"timestamp":"2017-01-21T10:56:15Z","locale":"en-GB",
		"intent":{"name":"CreateChallenge",
				"slots":{"Action":{"name":"Action","value":"increase"},
						 "Person":{"name":"Person","value":"Philipp"},
						 "Amount":{"name":"Amount","value":"5"}
						 }
				 }
	}
	}
	*/
	public static class Fu {
		public FuRequest request;
	}
	
	public static class FuRequest {
		public String requestId, locale;
		public Date timestamp;
		public FuRequest1 request;
	}

	public static class FuRequest1 {
		public String requestId, locale;
		public Date timestamp;
		public FuIntent intent;
	}
	
	public static class FuIntent {
		public String name;
		public FuSlots slots;
	}
	
	public static class FuSlots {
		public Map<String, String> Action = new HashMap<String, String>();
		public Map<String, String> Person = new HashMap<String, String>();
		public Map<String, String> Amount = new HashMap<String, String>();
		public Map<String, String> Betobject = new HashMap<String, String>();
	}
	
	public static class FuResponse {
		public String version = "1.0";
		public FuResponse1 response;
		
		public FuResponse() {
			response = new FuResponse1();
		}
		public FuResponse(String text) {
			response = new FuResponse1(text);
		}
	}
	
	public static class FuResponse1 {
		public FuResponse1() {
			outputSpeech.put("type", "PlainText");
			outputSpeech.put("text", "PlainText");
		}
		public FuResponse1(String text) {
			outputSpeech.put("type", "PlainText");
			outputSpeech.put("text", text);
		}

		public Map<String, String> outputSpeech = new HashMap<String, String>();
	}
	
}
