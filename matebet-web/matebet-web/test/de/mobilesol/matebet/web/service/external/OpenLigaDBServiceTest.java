package de.mobilesol.matebet.web.service.external;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import de.mobilesol.matebet.web.dto.GroupDTO;
import de.mobilesol.matebet.web.dto.LeagueDTO;
import de.mobilesol.matebet.web.dto.MatchDTO;

public class OpenLigaDBServiceTest {

	private OpenLigaDBService service = new OpenLigaDBService();

	@Test
	public void getMatches() {
		List<MatchDTO> matches = service.getMatches(848, 17750);
		System.out.println("matches #=" + matches.size());

		boolean found = false;
		for (MatchDTO m : matches) {
			System.out.println("m=" + m);

			Assert.assertTrue(m.groupId > 0);
			Assert.assertTrue(m.groupOrderId > 0);
			Assert.assertTrue(m.score1 >= 0);
			Assert.assertTrue(m.score2 >= 0);
			Assert.assertTrue(m.team1.teamId > 0);
			Assert.assertNotNull(m.team1.name);
			Assert.assertTrue(m.team2.teamId > 0);
			Assert.assertNotNull(m.team2.name);

			if (m.matchId == 33236) {
				Assert.assertEquals(m.groupId, 17750);
				Assert.assertEquals(m.groupOrderId, 1);
				Assert.assertEquals(m.score1, (Integer) 5);
				Assert.assertEquals(m.score2, (Integer) 0);
				Assert.assertEquals(m.team1.teamId, 40);
				Assert.assertEquals(m.team1.name, "Bayern München");
				Assert.assertEquals(m.team2.teamId, 100);
				Assert.assertEquals(m.team2.name, "Hamburger SV");

				found = true;
			}
		}

		Assert.assertTrue(found);
	}

	@Test
	public void getSport() {
		service.getSports();
	}

	@Test
	public void getLeagues() {
		List<LeagueDTO> leagues = service.getLeagues();
		Assert.assertTrue("size=" + leagues.size(), leagues.size() > 200);

		boolean found = false;
		for (LeagueDTO l : leagues) {

			Assert.assertTrue(l.leagueId > 0);
			Assert.assertNotNull(l.saison);
			Assert.assertNotNull(l.shortName);
			Assert.assertNotNull(l.title);

			if (l.leagueId == 848
					&& l.title.equals("1. Fußball-Bundesliga 2015/2016")) {
				found = true;
			}
		}

		Assert.assertTrue(found);
	}

	@Test
	public void getGroups() {
		List<GroupDTO> groups = service.getGroups(848);
		Assert.assertEquals(34, groups.size());
		for (GroupDTO g : groups) {

			Assert.assertEquals(g.leagueId, 848);
			Assert.assertTrue(g.groupId > 0);
			Assert.assertTrue(g.groupOrderId > 0);
			Assert.assertTrue(g.name, g.name.endsWith("Spieltag"));
			Assert.assertNotNull("2015");
			Assert.assertNotNull(g.firstMatchdate);
			Assert.assertNotNull(g.lastMatchdate);
			Assert.assertTrue(g.firstMatchdate + "<" + g.lastMatchdate,
					!g.firstMatchdate.after(g.lastMatchdate));
		}
	}

	@Test
	public void getMatchResult() {
		MatchDTO m = service.getMatchresult(33236);
		Assert.assertEquals(m.groupId, 17750);
		Assert.assertEquals(m.groupOrderId, 1);
		Assert.assertEquals(m.score1, (Integer) 5);
		Assert.assertEquals(m.score2, (Integer) 0);
	}
}
