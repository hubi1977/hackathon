package de.mobilesol.matebet.web.service;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import de.mobilesol.matebet.web.dto.BetDTO;
import de.mobilesol.matebet.web.dto.BetDTO.BetStatus;
import de.mobilesol.matebet.web.exception.MatebetValidationException;
import de.mobilesol.matebet.web.service.master.BetService;

@RunWith(MockitoJUnitRunner.class)
public class MatchTipServiceTest {

	@Mock
	private BetService betService;

	@InjectMocks
	private MatchTipService matchTipService;

	@Before
	public void setUp() {

		// betService = Mockito.mock(BetService.class);
		BetDTO b = new BetDTO();
		b.betId = 1;
		b.userIds = new ArrayList<Integer>();
		b.userIds.add(5);
		b.userIds.add(6);
		b.status = BetStatus.CLOSED;
		Mockito.when(betService.getBet(b.betId)).thenReturn(b);

		b = new BetDTO();
		b.betId = 2;
		b.userIds = new ArrayList<Integer>();
		b.userIds.add(5);
		b.userIds.add(6);
		b.status = BetStatus.FINISHED;
		Mockito.when(betService.getBet(b.betId)).thenReturn(b);

		b = new BetDTO();
		b.betId = 3;
		b.userIds = new ArrayList<Integer>();
		b.userIds.add(5);
		b.userIds.add(6);
		b.status = BetStatus.INITIATED;
		Mockito.when(betService.getBet(b.betId)).thenReturn(b);

		b = new BetDTO();
		b.betId = 10;
		b.userIds = new ArrayList<Integer>();
		b.userIds.add(5);
		b.userIds.add(6);
		b.status = BetStatus.RUNNING;
		Mockito.when(betService.getBet(b.betId)).thenReturn(b);
	}

	@Test(expected = MatebetValidationException.class)
	public void isClosed() {
		matchTipService.addMatchTips(1, 1, 1, null);
	}

	@Test(expected = MatebetValidationException.class)
	public void isFinished() {
		matchTipService.addMatchTips(2, 1, 1, null);
	}

	@Test(expected = MatebetValidationException.class)
	public void isInitiated() {
		matchTipService.addMatchTips(3, 1, 1, null);
	}
}
