# https://de.wikipedia.org/wiki/Liste_der_Nationalflaggen
# http://www.bundesliga.de/de/liga/tabelle/

for a in `ls icon*.png` ; do 
	convert -geometry 50x40 -quality 70 $a small/$a ;
done

for a in `ls icon*.svg` ; do 
	b=`echo $a | sed -e s/.svg/.png/g`;
	convert -geometry 50x40 -quality 70 $a small/$b ;
done

for a in `ls icon*.gif` ; do 
	b=`echo $a | sed -e s/.gif/.png/g`;
	convert -geometry 50x40 -quality 70 $a small/$b ;
done
